# Location Endpoint

## Locations
    
This Endpoint Will Get All the locations grouped by Countries, States, Cities and Regions.

    HTTP Verb : GET
    Parameters: None

### usage
    GET /api/v2/locations

Response
```    
    {
        "countries": [
                {
                    "id": 1,
                    "nameEn": "UAE",
                    "nameAr": "الإمارات"
                },
                {
                    "id": 2,
                    "nameEn": "Qatar",
                    "nameAr": "قطر\u200e\u200e"
                },
            
        ],
        "states": [
                {
                    "id": 1,
                    "countryId": 1,
                    "nameEn": "Abu Dhabi",
                    "nameAr": "أبو ظبي"
                },
                {
                    "id": 2,
                    "countryId": 1,
                    "nameEn": "Dubai",
                    "nameAr": "دبي"
                },
        ],
        "cities": [
                {
                    "id": 1,
                    "stateId": 1,
                    "nameEn": "Abu Dhabi City",
                    "nameAr": "مدينة أبو ظبي"
                },
                {
                    "id": 2,
                    "stateId": 1,
                    "nameEn": "Mussafah",
                    "nameAr": "المصفح"
                },
              
        ],
        "regions": [
                {
                    "id": 3,
                    "cityId": 1,
                    "nameEn": "Al Aman",
                    "nameAr": "الأمان"
                },
                {
                    "id": 4,
                    "cityId": 1,
                    "nameEn": "Al Bahya",
                    "nameAr": "الباهية"
                },
             
        ]
    }
```