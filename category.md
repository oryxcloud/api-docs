# Category Endpoint

## All Categories
    
This Endpoint Will Get All the categories grouped by Countries, States, Cities and Regions.

    HTTP Verb : GET
    Parameters: None

### usage
    GET /api/v2/categories

### Response
```    
    [
        {
            "isOpened": 0,
            "id": 1,
            "url": "properties",
            "parentId": null,
            "nameEn": "Properties",
            "nameAr": "عقارات",
            "hasChildren": 1
        },
        {
            "isOpened": 0,
            "id": 21,
            "url": "motors",
            "parentId": null,
            "nameEn": "Motors",
            "nameAr": "موتورات",
            "hasChildren": 1
        },
    ]
```

## Categories With Listings in a Country
    
This Endpoint Will Get All the categories which have atleast 1 listing in that category or its child category from a country

    HTTP Verb : GET
    Endpoint: api/v2/categories/country/{country_id}
    Parameters: {country_id} -> A Valid country id
### usage
    GET api/v2/categories/country/1

### Response
```    
    [
        {
            "isOpened": 0,
            "id": 1,
            "url": "properties",
            "parentId": null,
            "nameEn": "Properties",
            "nameAr": "عقارات",
            "hasChildren": 1
        },
        {
            "isOpened": 0,
            "id": 21,
            "url": "motors",
            "parentId": null,
            "nameEn": "Motors",
            "nameAr": "موتورات",
            "hasChildren": 1
        },
    ]
```
> An Invalid Country Id produces empty 200 response ie:[]

## Categories With Listings in a State
    
This Endpoint Will Get All the categories which have atleast 1 listing in that category or its child category from a state

    HTTP Verb : GET
    Endpoint: api/v2/categories/state/{state_id}
    Parameters: {state_id} -> A Valid state id
### usage
    GET api/v2/categories/state/5

### Response
```    
[
    {
        "isOpened": 0,
        "id": 1,
        "url": "properties",
        "parentId": null,
        "nameEn": "Properties",
        "nameAr": "عقارات",
        "hasChildren": 1
    },
    {
        "isOpened": 0,
        "id": 21,
        "url": "motors",
        "parentId": null,
        "nameEn": "Motors",
        "nameAr": "موتورات",
        "hasChildren": 1
    },
]
```
> An Invalid state Id produces empty 200 response ie:[]