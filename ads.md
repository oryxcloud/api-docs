# Ads Endpoint

## Search
    
This endpoint will give the search results from algolia

    HTTP Verb : GET
    Parameters: {*} -> Refer SearchController Parameters
    Endpoint: api/v2/ads/search

### usage
    GET /api/v2/ads/search

### Response
```    
{   
    "title": "All Categories in UAE (7409 ads)",
    "nextUrl": "http://uae.storat.local/api/v2/ads/search?page=2",
    "prevUrl": null,
    "currentPage": 1
    "results": [
        {
            "id": 139063,
            "price": 1100000,
            "discount_price": 0,
            "plan": 5,
            "feature": 1,
            "updated_at": {
                "date": "2018-01-26 23:50:10.000000",
                "timezone_type": 3,
                "timezone": "Asia/Dubai"
            },
            "created_at": {
                "date": "2018-01-12 08:41:53.000000",
                "timezone_type": 3,
                "timezone": "Asia/Dubai"
            },
            "published": 1,
            "deleted_at": 0,
            "under_review": 0,
            "user_id": 29573,
            "expired_at": 0,
            "is_from_xml": 0,
            "url": {
                "translations": {
                    "en": "http://dubai.tangoflip.com/properties/for-sale/apartments-for-sale/astthmr-fy-almydan-bdby-otmtaa-bajml-atlal-aaly-ajml-oatol-brjyn-sknyn-bmkdm-10-1",
                    "ar": "http://dubai.tangoflip.com/ar/properties/for-sale/apartments-for-sale/astthmr-fy-almydan-bdby-otmtaa-bajml-atlal-aaly-ajml-oatol-brjyn-sknyn-bmkdm-10-1"
                }
            },
            "translations": {
                "ar": {
                    "locale_id": 1,
                    "title": "استثمر في الميدان بدبي وتمتع بأجمل اطلالة علي اجمل واطول برجين سكنين بمقدم 10%",
                    "description": "بمقدم 10% فقط واقساط مريحة لحين الاستلام تملك شقه احلامك فى الميدان بافضل الاطلالات على اجمل واطول برجين سكنين فى العالم داخل دبى \r\nمشروع سكنى استثمارى مميز حيث يمثل رؤيه ثاقبه لوجهة عالمية مما يتيح اكبر فرصة للاستثمار المؤكد باعلى عائد داخل دبى \r\n\r\nالموقع \r\nيوجد المشروع فى منطقة الميدان وهى منطقة مميزة من حيث الموقع فهى تبعد عن الداون تاون وبرج خليفة اقل من 10 دقائق تبعد عن مطار ال مكتوم الدولى 15 دقيقة بالقرب من نادى الفروسية بمنطقة الميدان \r\n\r\nالخدمات والمميزات للمشروع\r\nحمامات سباحة \r\nجيم+سبا \r\nحديقة خاصة \r\nاماكن مخصصة لليوجا \r\nمناطق للشطرانج \r\nمناطق مخصصه للالعاب الرياضية على كورنيش القناة \r\n420 نافوره راقصة داخل المشروع\r\nاعلى مطعم بالعالم على اعلى برج سكنى بالعالم ·\r\nاطول مدرج تزلج بالعالم 1200 متر\r\nقناة مائية 2.5 ميل مع مارينا خاصة\r\nخط مترو جديد\r\nمساحة الشقة : 83م2 (غرفة وصالة وحمامين ومطبخ)\r\n\r\nللحجز والاستعلام"
                },
                "en": {
                    "locale_id": 2,
                    "title": "Apartment For Sale In Dubai - Enjoy The Most Beautiful View",
                    "description": "With only 10% installment and convenient installments until receipt, you have your dream apartment in the field with the best views of the most beautiful and tallest residential towers in the world within Dubai, a distinctive residential investment project which represents a vision for a global destination, providing the biggest opportunity for assured investment at the highest return within Dubai. It is a distinct area in terms of location. It is less than 10 minutes away from the Don Town and Burj Khalifa, away from the Al Maktoum International Airport. 15 minutes near Al-Midan Equestrian Club. Services and privileges for the project. Swimming pools. Gym. Private garden. Special areas for Yoga. Sports channel 420 Corniche fountain dancer inside Alparwaaaly restaurant in the world at the highest residential tower in the world · longest runway skiing world 1200 Mitrguenah Water 2.5 miles with Marina Khashkht Metro Jdidmessahh apartment: 83 m 2 (room, lounge, two bathrooms and a kitchen) for reservations and inquiries"
                }
            },
            "image": {
                "path": "listings/astthmr-fy-almydan-bdby-otmtaa-bajml-atlal-aaly-ajml-oatol-brjyn-sknyn-bmkdm-10-1-in-dby-5a5804525ba7f_original.jpeg",
                "width": 1000,
                "height": 456,
                "extension": "jpeg",
                "name": "astthmr-fy-almydan-bdby-otmtaa-bajml-atlal-aaly-ajml-oatol-brjyn-sknyn-bmkdm-10-1-in-dby-5a5804525ba7f",
                "dominant_color": "#675c4f"
            },
            "location": {
                "country": {
                    "id": 1,
                    "translations": {
                        "ar": {
                            "name": "الإمارات",
                            "locale_id": 1
                        },
                        "en": {
                            "name": "UAE",
                            "locale_id": 2
                        }
                    }
                },
                "state": {
                    "id": 2,
                    "translations": {
                        "ar": {
                            "name": "دبي",
                            "locale_id": 1
                        },
                        "en": {
                            "name": "Dubai",
                            "locale_id": 2
                        }
                    }
                },
                "city": {
                    "id": 5,
                    "translations": {
                        "ar": {
                            "name": "دبي",
                            "locale_id": 1
                        },
                        "en": {
                            "name": "Dubai",
                            "locale_id": 2
                        }
                    }
                },
                "region": {
                    "id": 205,
                    "translations": {
                        "en": {
                            "name": "Dubai",
                            "locale_id": 2
                        },
                        "ar": {
                            "name": "دبي",
                            "locale_id": 1
                        }
                    }
                }
            },
            "categories": {
                "id": 12,
                "url": "properties/for-sale/apartments-for-sale",
                "translations": {
                    "en": {
                        "name": "Apartments for Sale",
                        "locale_id": 2
                    },
                    "ar": {
                        "name": "شقق للبيع",
                        "locale_id": 1
                    }
                }
            },
            "custom_attributes": {
                "10": {
                    "value": 83
                },
                "11": {
                    "value": 0
                },
                "12": {
                    "value": 1
                },
                "2-1": {
                    "value": "on"
                },
                "13-4": {
                    "value": "on"
                },
                "13-7": {
                    "value": "on"
                },
                "13-8": {
                    "value": "on"
                }
            },
            "verified": {
                "user_type": "individual",
                "mobileVerified": 1,
                "emailVerified": 1,
                "idVerified": 0,
                "businessVerified": 0
            },
            "store": {
                "logo": [],
                "hasLogo": 0,
                "name": {
                    "en": "Wasaieef's Store",
                    "ar": null
                }
            },
            "created_at_timestamp": 1515732113,
            "updated_at_timestamp": 1516996210,
            "_tags": [
                {
                    "id": 12,
                    "name": "properties/for-sale/apartments-for-sale",
                    "url": "properties/for-sale/apartments-for-sale",
                    "original_name": "Apartments for Sale"
                },
                {
                    "id": 11,
                    "name": "properties/for-sale",
                    "url": "properties/for-sale",
                    "original_name": "For Sale"
                },
                {
                    "id": 1,
                    "name": "properties",
                    "url": "properties",
                    "original_name": "Properties"
                }
            ],
            "_tags_ar": [
                {
                    "id": 12,
                    "name": "properties/for-sale/apartments-for-sale",
                    "url": "properties/for-sale/apartments-for-sale",
                    "original_name": "شقق للبيع"
                },
                {
                    "id": 11,
                    "name": "properties/for-sale",
                    "url": "properties/for-sale",
                    "original_name": "للبيع"
                },
                {
                    "id": 1,
                    "name": "properties",
                    "url": "properties",
                    "original_name": "عقارات"
                }
            ],
            "objectID": "139063",
            "_highlightResult": {
                "translations": {
                    "ar": {
                        "title": {
                            "value": "استثمر في الميدان بدبي وتمتع بأجمل اطلالة علي اجمل واطول برجين سكنين بمقدم 10%",
                            "matchLevel": "none",
                            "matchedWords": []
                        }
                    },
                    "en": {
                        "title": {
                            "value": "Apartment For Sale In Dubai - Enjoy The Most Beautiful View",
                            "matchLevel": "none",
                            "matchedWords": []
                        }
                    }
                },
                "location": {
                    "country": {
                        "translations": {
                            "ar": {
                                "name": {
                                    "value": "الإمارات",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            },
                            "en": {
                                "name": {
                                    "value": "UAE",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            }
                        }
                    },
                    "state": {
                        "translations": {
                            "ar": {
                                "name": {
                                    "value": "دبي",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            },
                            "en": {
                                "name": {
                                    "value": "Dubai",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            }
                        }
                    },
                    "city": {
                        "translations": {
                            "ar": {
                                "name": {
                                    "value": "دبي",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            },
                            "en": {
                                "name": {
                                    "value": "Dubai",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            }
                        }
                    },
                    "region": {
                        "translations": {
                            "en": {
                                "name": {
                                    "value": "Dubai",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            },
                            "ar": {
                                "name": {
                                    "value": "دبي",
                                    "matchLevel": "none",
                                    "matchedWords": []
                                }
                            }
                        }
                    }
                },
                "categories": {
                    "translations": {
                        "en": {
                            "name": {
                                "value": "Apartments for Sale",
                                "matchLevel": "none",
                                "matchedWords": []
                            }
                        },
                        "ar": {
                            "name": {
                                "value": "شقق للبيع",
                                "matchLevel": "none",
                                "matchedWords": []
                            }
                        }
                    }
                },
                "store": {
                    "name": {
                        "en": {
                            "value": "Wasaieef's Store",
                            "matchLevel": "none",
                            "matchedWords": []
                        }
                    }
                }
            },
            "title": "Apartment For Sale In Dubai - Enjoy The Most Beautiful View",
            "description": "With only 10% installment and convenient installments until receipt, you have your dream apartment in the field with the best views of the most beautiful and tallest residential towers in the world within Dubai, a distinctive residential investment project which represents a vision for a global destination, providing the biggest opportunity for assured investment at the highest return within Dubai. It is a distinct area in terms of location. It is less than 10 minutes away from the Don Town and Burj Khalifa, away from the Al Maktoum International Airport. 15 minutes near Al-Midan Equestrian Club. Services and privileges for the project. Swimming pools. Gym. Private garden. Special areas for Yoga. Sports channel 420 Corniche fountain dancer inside Alparwaaaly restaurant in the world at the highest residential tower in the world · longest runway skiing world 1200 Mitrguenah Water 2.5 miles with Marina Khashkht Metro Jdidmessahh apartment: 83 m 2 (room, lounge, two bathrooms and a kitchen) for reservations and inquiries"
        }
    ]
}
```

## Get Single Listing
    
This endpoint will fetch the details of a given listing id

    HTTP Verb : GET
    Parameters: {listing_id}
    Endpoint: api/v2/ads/listing/{listing_id}

### usage
    GET /api/v2/ads/listing/3217

### Response
```    
{
    "id": 3217,
    "type": "",
    "price": "-1.00",
    "discount_price": "0.00",
    "currency_code": 1,
    "url": "/services/construction/events-fences-dubai-outdoor-wooden-fences-picket-white-fences-wooden-fence-1",
    "is_enabled": 1,
    "is_spam": 0,
    "store_id": 0,
    "user_id": 1575,
    "category_id": 1756,
    "created_at": "2016-09-01 12:08:23",
    "updated_at": "2016-09-01 12:08:23",
    "published": 1,
    "reference_no": null,
    "source_url": null,
    "hide_phone": 0,
    "hide_email": 1,
    "deleted_at": null,
    "youtube_link": "",
    "showOnHomepage": 0,
    "youtube_url": "",
    "plan": 1,
    "feature": 0,
    "fb_campaign_id": null,
    "enable_analytics": 0,
    "enabled_in_search": 1,
    "is_from_xml": 0,
    "under_review": 0,
    "show_website": 1,
    "show_store": 1,
    "bulk_upload_id": null,
    "promotion_ends_at": null,
    "expired_at": null,
    "current_translation": [
        {
            "id": 3303,
            "locale_id": 2,
            "title": "Events Fences Dubai | Outdoor Wooden Fences | Picket White Fences | Wooden Fence",
            "description": "DESERT DREAMS DECOR Manufacturer and Install Various types of wooden fence like picket fence, garden fence, swimming pool fence, public beach fences, parks fence, events fence, kids privacy fence, balcony fence, free standing fence, white fence, rainbow color’s fence, slat fence, stadium fence, outdoor fence , indoor fence, wooden cabins and wooden gates according to our clients needs and preferences.  \r\nMany homeowners appreciate the classic look and feel of wood fencing.",
            "listings_id": 3217,
            "created_at": "2016-09-01 12:08:23",
            "updated_at": "2016-09-01 12:08:23"
        }
    ],
    "user": {
        "id": 1575,
        "email": "desertdreamsdecoruae@gmail.com",
        "verified": 1,
        "verified_at": null,
        "enabled": 1,
        "active": 0,
        "api_token": "cZnt6Vw7abF66ZMPx2SmWgln",
        "created_at": "2016-06-01 14:55:16",
        "updated_at": "2016-09-01 11:15:38",
        "about": "",
        "contact_email": "",
        "first_name": "Desert Dreams Decor and",
        "last_name": "Furniture",
        "type": "Business",
        "latitude": "",
        "longitude": "",
        "zoom": "",
        "mobile": "+971526713737",
        "mobile_verified": 1,
        "admin": 0,
        "blocked": 0,
        "flagged": 0,
        "plan": 0,
        "last_login": null,
        "email_subscribed": 0,
        "lead_plan": 0,
        "id_submitted": 0,
        "id_verified": 0,
        "flagged_at": null,
        "send_leads_by_sms": 0,
        "type_determined": 0,
        "call_recording_enabled": 0
    },
    "title": "Events Fences Dubai | Outdoor Wooden Fences | Picket White Fences | Wooden Fence",
    "description": "DESERT DREAMS DECOR Manufacturer and Install Various types of wooden fence like picket fence, garden fence, swimming pool fence, public beach fences, parks fence, events fence, kids privacy fence, balcony fence, free standing fence, white fence, rainbow color’s fence, slat fence, stadium fence, outdoor fence , indoor fence, wooden cabins and wooden gates according to our clients needs and preferences.  \r\nMany homeowners appreciate the classic look and feel of wood fencing.",
    "images": [
        "https://storat-local.s3.eu-west-1.amazonaws.com/listings/57c81973d9105_original.jpeg",
        "https://storat-local.s3.eu-west-1.amazonaws.com/listings/57c8197576f35_original.jpeg",
    ]
}
```
> An Invalid {listing_id} will producea an html 404 response with 404 response code
