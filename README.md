# **API Documention**
This Repository Contains the documentation for Apiv2 for storat.com
 - Currently Supports English Version


 
## Contents
---
1. [Location Endpoint](location.md)
2. [Category Endpoint](category.md)
3. [Attributes Endpoint](attributes.md)
4. [Ads Endpoint](ads.md)
5. [Contact Seller](contact.md)
6. [Register](register.md)
7. [Verification](verification.md)
8. [Misc](misc.md)