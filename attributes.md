# Attributes Endpoint

## Attributes for a Category
    
This endpoint will give the category attributes

    HTTP Verb : GET
    Parameters: {category_id} -> ID of the Category
    Endpoint: api/v2/attributes/{category_id}

### usage
    GET /api/v2/attributes/2

### Response
```    
{
    "category_attributes": [
        {
            "id": 9,
            "category_id": 1,
            "attribute_id": 2,
            "validation_rules": [],
            "created_at": "2018-03-27 21:37:56",
            "updated_at": "2018-03-27 21:37:56",
            "attribute": {
                "id": 2,
                "type": "Drop Down",
                "created_at": "2018-03-27 21:37:55",
                "updated_at": "2018-03-27 21:37:55",
                "type_search": "Drop Down",
                "validation_rules": [],
                "url_format": null,
                "url_regex": null,
                "url_format_ar": null,
                "url_regex_ar": null,
                "current_translation": [
                    {
                        "id": 3,
                        "attribute_id": 2,
                        "locale_id": 2,
                        "name": "Listed By",
                        "options": [
                            "Agent",
                            "Owner",
                            " Developer"
                        ],
                        "created_at": "2018-03-27 21:37:55",
                        "updated_at": "2018-03-27 21:37:55"
                    }
                ],
                "translations": [
                    {
                        "id": 3,
                        "attribute_id": 2,
                        "locale_id": 2,
                        "name": "Listed By",
                        "options": [
                            "Agent",
                            "Owner",
                            " Developer"
                        ],
                        "created_at": "2018-03-27 21:37:55",
                        "updated_at": "2018-03-27 21:37:55"
                    },
                    {
                        "id": 4,
                        "attribute_id": 2,
                        "locale_id": 1,
                        "name": "المعلن عنها",
                        "options": [
                            "وكيل ",
                            " المالك ",
                            "المطور العقاري"
                        ],
                        "created_at": "2018-03-27 21:37:55",
                        "updated_at": "2018-03-27 21:37:55"
                    }
                ],
                "name": "Listed By",
                "options": [
                    "Agent",
                    "Owner",
                    " Developer"
                ]
            }
        },    
        {
            "id": 72,
            "category_id": 2,
            "attribute_id": 48,
            "validation_rules": {
                "type": "integer",
                "min": "1"
            },
            "created_at": "2018-03-27 21:37:57",
            "updated_at": "2018-03-27 21:37:57",
            "attribute": {
                "id": 48,
                "type": "Range",
                "created_at": "2018-03-27 21:37:56",
                "updated_at": "2018-03-27 21:37:56",
                "type_search": "Range",
                "validation_rules": {
                    "type": "integer",
                    "min": "1"
                },
                "url_format": null,
                "url_regex": null,
                "url_format_ar": null,
                "url_regex_ar": null,
                "current_translation": [
                    {
                        "id": 63,
                        "attribute_id": 48,
                        "locale_id": 2,
                        "name": "Cheques",
                        "options": [],
                        "created_at": "2018-03-27 21:37:56",
                        "updated_at": "2018-03-27 21:37:56"
                    }
                ],
                "translations": [
                    {
                        "id": 63,
                        "attribute_id": 48,
                        "locale_id": 2,
                        "name": "Cheques",
                        "options": [],
                        "created_at": "2018-03-27 21:37:56",
                        "updated_at": "2018-03-27 21:37:56"
                    },
                    {
                        "id": 64,
                        "attribute_id": 48,
                        "locale_id": 1,
                        "name": "الشيكات",
                        "options": [],
                        "created_at": "2018-03-27 21:37:56",
                        "updated_at": "2018-03-27 21:37:56"
                    }
                ],
                "name": "Cheques",
                "options": []
            }
        }
    ]
}
```

