# Mobile Verification Endpoint

## Send Verification Code
    
This endpoint will send a verification code to a users mobile

    HTTP Verb : POST
    Parameters: ['api_token']
    Endpoint: api/v2/mobile/send-code

### usage
    POST /api/v2/mobile/send-code

**cURL**
```
    curl -X POST \
    http://uae.storat.local/api/v2/mobile/send-code \
    -H 'Cache-Control: no-cache' \
    -H 'Postman-Token: 5ff7d329-190a-4108-af82-34caaa9d2f29' \
    -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
    -F api_token=57A3NIykUZgM7vKbBpxn0F0O
```
### Response
```   
{
    "meta": {
        "code": 200,
        "message": "Verification message sent to +971509574578"
    }
}
```

> Already Verified Response 
```
{
    "meta": {
        "code": 200,
        "message": "Already Verified"
    }
}
```

> Invalid api_token will produce a 401 error
```   
{
    "meta": {
        "code": 401,
        "error": {
            "type": "Unauthorized",
            "messsage": "Invalid user credentials"
        }
    }
}
```
> Any Other errors will produce a 50X Response code

## Verify Mobile Code
    
This endpoint will verify a verification code sent to a users mobile

    HTTP Verb : POST
    Parameters: ['api_token','v-code']
    Endpoint: api/v2/mobile/verify-code

### usage
    POST /api/v2/mobile/verify-code

**cURL**
```
curl -X POST \
    http://uae.storat.local/api/v2/mobile/verify-code \
    -H 'Cache-Control: no-cache' \
    -H 'Postman-Token: cc4a0225-b7ea-480d-8089-89ee65d22263' \
    -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
    -F api_token=57A3NIykUZgM7vKbBpxn0F0O \
    -F v-code=22810
```
### Response
```   
{
    "meta": {
        "code": 200,
        "message": "You have successfully verified your mobile phone"
    }
}
```

> Any Other errors will produce a 50X Response code


