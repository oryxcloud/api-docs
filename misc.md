# Mobile Verification Endpoint

## Get User Status
    
This endpoint will give the users status

    HTTP Verb : GET
    Parameters: {api_token}
    Endpoint: api/v2/user/status/{api_token}

### usage
    GET /api/v2/user/status/57A3NIykUZgM7vKbBpxn0F0O

### Response
```   
{
    "meta": {
        "code": 200
    },
    "data": {
        "user": {
            "email_verified": false,
            "mobile_verified": true,
            "id_verified": false,
            "active": false,
            "enabled": true,
            "blocked": false,
            "flagged": false
        }
    }
}
```

> Invalid APi token will produce 200 response but with following response
```
{
    "meta": {
        "code": 404
    },
    "data": {
        "errors": "User not found"
    }
}
```

## Send EMail Voucher
