# Contact Endpoint

## Contact seller
    
This endpoint will save the details of the buyer as a lead. 

    HTTP Verb : POST
    Parameters: {listing_id},['user_name', 'user_email', 'user_phone', 'message']
    Endpoint: api/v2/ads/{listing_id}/contact

### usage
    POST /api/v2/ads/3217/contact

### Response
```   
{
    "meta": {
        "code": 200
    },
    "message": "Message sent successfully"
}
```
> Invalid Listing Id Produces an html 404 response with 404 status code,

> Invalid parameters produces a 400 response code.
```   
{
    "meta": {
        "code": 400
    },
    "message": "Validation Failed"
}
```