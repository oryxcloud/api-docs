# Register Endpoint

## Register a User
    
This endpoint will save the details of the user. 

    HTTP Verb : POST
    Parameters: ['first_name', 'last_name', 'email', 'password', 'mobile', 'terms']
    Endpoint: api/v2/register

### usage
    POST /api/v2/register

**cURL**
```
    curl -X POST \
        http://uae.storat.local/api/v2/register \
        -H 'Cache-Control: no-cache' \
        -H 'Postman-Token: eb88d2dd-ecfc-4bc6-8615-df6343079ffa' \
        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
        -F first_name=Shobi \
        -F last_name=Test \
        -F mobile=132156645 \
        -F email=asjfhdjkdj@cc.com \
        -F password=test1234 \
        -F terms=1
```
### Response
```   
{
    "meta": {
        "code": 200
    },
    "data": {
        "user": {
            "email": "asjfhdjkdj@cc.com",
            "first_name": "Shobi",
            "last_name": "Test",
            "mobile": "132156645",
            "type": "individual",
            "api_token": "F0WnN4gubT7wJRnxQyQ4AsVL",
            "updated_at": "2018-04-05 12:37:05",
            "created_at": "2018-04-05 12:37:04",
            "id": 31966,
            "last_login": {
                "date": "2018-04-05 12:37:05.606195",
                "timezone_type": 3,
                "timezone": "Asia/Dubai"
            }
        }
    }
}
```

> Invalid parameters produces a 400 response code.
```   
{
    "meta": {
        "code": 400
    },
    "data": {
        "errors": {
            "first_name": [
                "The first name field is required."
            ],
            "last_name": [
                "The last name field is required."
            ],
            "email": [
                "The email has already been taken."
            ]
        }
    }
}
```
> Any Other errors will produce a 50X

## Register a User Via Social Login
    
This endpoint will save the details of the user with minimal data aquired from social media. 

    HTTP Verb : POST
    Parameters: ['firstName', 'lastName', 'email']
    Endpoint: api/v2/social/register

### usage
    POST /api/v2/social/register

**cURL**
```
    curl -X POST \
        http://uae.storat.local/api/v2/social/register \
        -H 'Cache-Control: no-cache' \
        -H 'Postman-Token: ebc7a523-52f5-4bd5-9344-d8075fcb54ff' \
        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
        -F firstName=Shobi \
        -F lastName=Test \
        -F email=asjfhdjkdj@cc.coms
```
### Response
```   
{
    "meta": {
        "code": 200,
        "message": "User Registered"
    },
    "data": {
        "user": {
            "first_name": "Shobi",
            "last_name": "Test",
            "email": "asjfhdjkdj@cc.coms",
            "type": "Business",
            "api_token": "EBNw2cDg1xV9vJgZVWNpVpBI",
            "updated_at": "2018-04-05 12:43:15",
            "created_at": "2018-04-05 12:43:14",
            "id": 31967,
            "last_login": {
                "date": "2018-04-05 12:43:15.805826",
                "timezone_type": 3,
                "timezone": "Asia/Dubai"
            },
            "store": {
                "id": 31970,
                "locale_code": 0,
                "name": "Shobi's Store",
                "url": "",
                "currency": "1",
                "countries_id": 1,
                "user_id": 31967,
                "created_at": "2018-04-05 12:43:14",
                "updated_at": "2018-04-05 12:43:14",
                "store_types_id": 1,
                "state_id": null,
                "city_id": null,
                "region_id": null,
                "addressline1": "",
                "addressline2": "",
                "addressline3": "",
                "store_email": "",
                "store_phone": "",
                "website": null,
                "store_category_id": null,
                "facebook": "",
                "twitter": "",
                "speciality": "",
                "linkedin": "",
                "instagram": "",
                "hide_email": 0,
                "hide_phone": 0,
                "hide_location": 0,
                "hide_facebook": 0,
                "hide_twitter": 0,
                "hide_linkedin": 0,
                "promotedOnDirectory": 0,
                "isFeaturedOnHomepage": 0,
                "featuredDate": "0000-00-00 00:00:00",
                "google": "",
                "youtube": "",
                "virtual_number_id": null,
                "enable_virtual_number_in_website": 0,
                "lead_price": null,
                "current_translation": [
                    {
                        "id": 36434,
                        "locale_id": 2,
                        "store_id": 31970,
                        "description": "",
                        "created_at": "2018-04-05 12:43:15",
                        "updated_at": "2018-04-05 12:43:15",
                        "name": "Shobi's Store",
                        "speciality": null
                    }
                ],
                "description": ""
            }
        }
    }
}
```

> Invalid parameters produces a 400 response code.
```   
{
    "meta": {
        "code": 400
    },
    "data": {
        "errors": {
            "firstName": [
                "The first name field is required."
            ],
            "lastName": [
                "The last name field is required."
            ],
            "email": [
                "The email has already been taken."
            ]
        }
    }
}
```

> Api will produce 401, if email is blocked and any other error will be 50x